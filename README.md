
## Installation

Install virtualenv
```
pip install virtualenv
```
On the project directory,

Create virtual environment
```
virtualenv -p python3 venv
```
Activate
```
source venv/bin/activate
```

Install requirements
```
pip install -r requirements.txt
```

## Run

just set image path as argument and run following command
```
python scan.py [imgpath]
```
